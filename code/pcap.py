import pcap

def sniff():
	p = pcap.pcapObject()
    dev = "en0"   # here the interface to sniff
    net, mask = pcap.lookupnet(dev)
    p.open_live(dev, 1600, 0, 100)
    p.setfilter("tcp src port 443", 0, 0)
    try:
        while 1:
            p.dispatch(1, decode_packet)
    except KeyboardInterrupt:
        scheduler.shutdown()
        print '%s' % sys.exc_type
        print 'shutting down'
        print '%d packets received, %d packets dropped, %d packets \
                dropped by interface' % p.stats()
def decode_packet(pktlen, data, timestamp):
	#code to handle the packet capture.
	pass
if __name__ == '__main__':
	sniff()
