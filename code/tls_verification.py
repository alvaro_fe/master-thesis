
class TLSVerificationDispatch():
    def __init__(self, data):

        self.certs = None
        self.status_request = None
        if 'cert' in data:
            self.certs = data['cert']
        if 'status_request' in data:
            self.status_request = data['status_request']
        self.dispatch_certificate()
        self.dispatch_status_request()

    def dispatch_certificate(self):
        # Do everything related with certificate
        if self.certs is not None:
            try:
                chain = X509Chain(self.certs)
            except Exception as e:
                print e
                return
            if chain.length_chain() == 1:
                try:
                    print '[-] Chain incomplete from %s' % chain.ca_name()
                except Exception:
                    print '[-] Chain incomplete'
            else:
                ocsp = Ocsp(chain)
                debug_logger.debug('[+] Verifying certificate')
                for cls in handlers.store:
                    instance = handlers.store[cls](chain, ocsp)
        else:
            pass

    def dispatch_status_request(self):
        if self.dispatch_status_request is not None:
            # verify connection through ocsp_stapling
            # In the future only add here all the code needed
            pass
        else:
            pass