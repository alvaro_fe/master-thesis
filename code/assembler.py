def is_initial_record(data):
    try:
        (content_type, version_mayor,
        version_minor,length, msg_type
        ) = struct.unpack_from("!BBBHB", data, 0)
    except:
        # It isn't a tls_record
        return False
    if content_type == tls_types.TLS_HANDSHAKE and \
       msg_type == tls_types.TLS_H_TYPE_SERVER_HELLO:
        return True
    elif content_type == tls_types.TLS_ALERT and \
            version_mayor == 3 and (version_minor == 1 or version_minor == 3):
        return True
    else:
        return False

tls_data = dict()

def assembler(data, s_addr, d_addr, source_port, dest_port, flag, sequence):
    sport = str(source_port)
    dport = str(dest_port)
    recollect = False
    id = (s_addr, d_addr, sport, dport)
    try:
        recollect = tls_data[id]['recollect']
    except KeyError:
        recollect = False

    if flag == 16:
        # 16 means ACK
        if recollect is True:
            tls_data[id]['data'][sequence] = data

        elif is_initial_record(data):
            tls_data[id] = dict()
            tls_data[id]['data'] = dict()
            tls_data[id]['recollect'] = True
            tls_data[id]['psh-ack'] = 0
            tls_data[id]['data'][sequence] = data
    if flag == 24:
        # 24 means PSH-ACK
        if recollect is True:
            if tls_data[id]['psh-ack'] != 1:
                tls_data[id]['psh-ack'] += 1
                tls_data[id]['data'][sequence] = data
            else:
                # Second PSH-ACK received
                tls_data[id]['data'][sequence] = data
                stream = tls_data[id]['data']
                TLSStream(stream)
                del (tls_data[id])
