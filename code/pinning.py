@handler(handlers, isHandler=config.V_PINNING)
class Pinning(BaseHandler):

    name = "pinning"

    def __init__(self, cert, ocsp):
        super(Pinning, self).__init__()
        self.on_certificate(cert)

    def on_certificate(self, cert):
        name = cert.subject_common_name()
        issuer_name = cert.issuer_common_name()
        issuers = db.get(name)
        if issuers is None:
            print "\t[-] You have not pinned this certificate %s" % name
            return
        try:
            spki = cert.hash_spki(1)
        except:
            logger.error("Getting spki of the intermediate CA %s" % name)
            return
        try:
            issuers = issuers["issuers"]
            for i in issuers[issuer_name]:
                if spki == i:
                    print "\t[+] pin correct %s " % name
                    return
            logger.info("\t[-] Pin does not match %s" % name)
            print "\t[-] Pin does not match %s" % name
            MITMNotification.notify(title="pinning",
                message=cert.subject_common_name())
        except:
            print "\t[-] %s" % sys.exc_info()[0]
