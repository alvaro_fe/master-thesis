@handler(handlers, isHandler=config.V_KEYCONTINUITY)
class KeyContinuity(BaseHandler):

    name = "keycontinuity"

    def __init__(self, cert, ocsp):
        super(KeyContinuity, self).__init__()
        self.on_certificate(cert)

    def on_certificate(self, cert):
        hash_t = cert.hash_spki(algorithm="sha256")
        name = cert.subject_common_name()
        algorithm = cert.get_cert_nss().subject_public_key_info.algorithm
        algorithm = algorithm.id_str
        _id = algorithm + ' - ' + name
        exist = db.get(_id)
        if exist is None:
            # That means that the certificate is not in the db, it's the
            # first time it was seen
            db.set_hash(hash_t, _id)
            print "\t[+] Certificate %s first seen" % name
        else:
            # Exist so we have to ensure that it is correct
            correct = db.compare(_id, hash_t)
            if correct is False:
                before = db.get(_id)
                print "\t[-] Certificate %s has changed" % name
                MITMNotification.notify(title=self.name, message=name)
            else:
                print "\t[+] Certificate %s has not changed" % name
