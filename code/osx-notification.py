from pync import Notifier
from notification.event_notification import IObserver

class NotificationOSX(IObserver):

    def notify(self, *args, **kw):
        message_l = None
        title_l = None
        keys = kw.keys()
        if "message" in keys:
            message_l = kw["message"]
        if "title" in keys:
            title_l = kw["title"]
        Notifier.notify(title_l, title=message_l)

