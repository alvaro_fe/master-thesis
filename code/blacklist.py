
@handler(handlers, isHandler=config.V_BLACKLIST)
class Blacklist(BaseHandler):

    name = "blacklist"

    def __init__(self, cert, ocsp):
        super(Blacklist, self).__init__(cert, ocsp)
        self.on_certificate(cert)

    def on_certificate(self, cert):
        name = cert.ca_name()
        fingerprint = cert.hash()
        query = db.get(fingerprint)
        if query is None:
            print "\t[+] Certificate %s is safe against blacklist" % name
        else:
            print "\t[-] Certificate %s match with a malware site" % name
            MITMNotification.notify(
                title=self.name,
                message=cert.subject_common_name()
                )

db = BlackListDB(config.DB_NAME, Blacklist.name)
